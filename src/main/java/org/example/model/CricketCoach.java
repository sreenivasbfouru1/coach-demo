package org.example.model;

import lombok.*;
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Builder

public class CricketCoach implements Coach {
    private String coachId;
    private String coachName;
    @Override
    public String dailyWorkout() {

        return "practise spin Bowling";
    }
}
